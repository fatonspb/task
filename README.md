## Installing

pull code, build container, install vendors and run migrations

```
git clone https://bitbucket.org/fatonspb/task.git
cd task
docker-compose up -d
```
Wait 5-7 seconds to MySql wake up.
```
docker exec -it task_php_1 php bin/console.php migration
```

## Using

Open http://localhost:88

## Additional
Use port `3307` if you want to connect to MySql
