<?php

namespace App\Service;

use App\Entity\Datum;
use App\Repository\DatumRepository;
use App\Service\DbDriver\DbDriverInterface;

class DatumService
{
    /**
     * @var DbDriverInterface
     */
    private $driver;
    /**
     * @var DatumRepository
     */
    private $datumRepository;

    public function __construct(
        DbDriverInterface $driver,
        DatumRepository $datumRepository
    ) {
        $this->driver = $driver;
        $this->datumRepository = $datumRepository;
    }

    /**
     * Save new entity
     *
     * @param int $id
     * @param string $secret
     * @return bool
     */
    public function create(int $id, string $secret):bool
    {
        if (!$this->datumRepository->find(['id' => $id])) {
            $data = [
                'id' => $id,
                'secret' => $secret,
            ];

            return $this->driver->create(Datum::TABLE_NAME, $data);
        } else {
            // entity exist
            return false;
        }
    }
}