<?php

namespace App\Service\DbDriver;

interface DbDriverInterface
{
    /**
     * @param string $className
     * @param string $tableName
     * @param array $criteria
     * @param string|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function find(
        string $className,
        string $tableName,
        array $criteria,
        string $orderBy = null,
        int $limit = null,
        int $offset = null
    ): array;

    /**
     * @param string $sql
     * @return mixed
     */
    public function runSql(string $sql);

    /**
     * @param string $tableName
     * @param array $values
     * @return mixed
     */
    public function create(string $tableName, array $values): bool;
}