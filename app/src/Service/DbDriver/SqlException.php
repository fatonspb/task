<?php

namespace App\Service\DbDriver;

use App\AppException;

class SqlException extends AppException
{
}