<?php

namespace App\Service\DbDriver;

use mysqli;

class MySql implements DbDriverInterface
{
    /**
     * @var mysqli
     */
    private $connection;

    public function __construct()
    {
        $this->connection = $this->getConnection();
    }

    /**
     * @return mysqli
     */
    public function getConnection(): mysqli
    {
        $dbhost = getenv('DB_HOST');
        $dbuser = getenv('MYSQL_USER');
        $dbpass = getenv('MYSQL_PASSWORD');
        $db = getenv('MYSQL_DATABASE');
        $conn = new mysqli($dbhost, $dbuser, $dbpass, $db);

        if ($conn->connect_errno) {
            throw new SqlException('SQL connection fail (' . $conn->connect_errno . ') ' . $conn->connect_error);
        }

        return $conn;
    }

    /**
     * @param string $className
     * @param string $tableName
     * @param array $criteria
     * @param string $orderBy
     * @param int $limit
     * @param int $offset
     * @return array
     * @throws SqlException
     */
    public function find(
        string $className,
        string $tableName,
        array $criteria,
        string $orderBy = null,
        int $limit = null,
        int $offset = null
    ):array {
        $sql = $this->buildSelectSql($tableName, $criteria, $orderBy, $limit, $offset);

        $result = [];
        $res = $this->runSql($sql);

        while ($obj = $res->fetch_object($className)) {
            $result[] = $obj;
        }

        $res->close();

        return $result;
    }

    /**
     * {@inheritDoc}
     */
    public function create(string $tableName, array $values): bool
    {
        $sql['insertPart'] = 'INSERT INTO `' . $tableName . '`';
        $sql['fieldsPart'] = '(' . implode(', ', $this->wrapArrayItems(array_keys($values), '`')) . ')';
        $sql['valuesPart'] = 'VALUES (' . implode(', ', $this->wrapArrayItems(array_values($values))) . ')';

        return $this->runSql(implode(' ', $sql) . ';');
    }

    /**
     * {@inheritDoc}
     */
    public function runSql(string $sql) {
        $result = $this->connection->query($sql);

        if (!$result) {
            throw new SqlException('Run sql fail: ' . $this->connection->errno . ': ' . $this->connection->error . ' $sql = "' . $sql . '"');
        }

        return $result;
    }

    /**
     * @param string $tableName
     * @param array $criteria
     * @param string $orderBy
     * @param int $limit
     * @param int $offset
     * @return string
     * @throws SqlException
     */
    private function buildSelectSql(
        string $tableName,
        array $criteria,
        string $orderBy = null,
        int $limit = null,
        int $offset = null
    ): string {
        $conditions = [];

        foreach ($criteria as $paramName => $paramValue) {
            if ($paramValue === null) {
                $conditions[] = sprintf('`%s` IS NULL', $paramName);
            } elseIf (is_array($paramValue)) {
                $paramValueString = implode(', ', $this->wrapArrayItems($paramValue));
                $conditions[] = sprintf('`%s` IN (%s)', $paramName, $paramValueString);
            } else {
                $conditions[] = sprintf('`%s` = "%s"', $paramName, $paramValue);
            }
        }

        $sql['selectPart'] = 'SELECT *';

        $sql['fromPart'] = 'FROM ' . $tableName;

        $sql['conditions'] = !empty($conditions)
            ? 'WHERE ' . implode(' AND ', $conditions)
            : '';

        $sql['orderPart'] = $orderBy !== null
            ? 'ORDER BY ' . $orderBy
            : '';

        $sql['limitOffsetPart'] = $limit !== null
            ? 'LIMIT ' . ($offset ? $offset . ',' : '') . $limit
            : '';

        return implode("\n", array_filter($sql)) . ';';
    }

    /**
     * @param array $paramValue
     * @param string $symbol
     * @return array
     */
    private function wrapArrayItems(array $paramValue, string $symbol = '\''): array
    {
        return array_map(
            static function ($param) use ($symbol) {
                return $symbol . $param . $symbol;
            }, $paramValue
        );
    }
}