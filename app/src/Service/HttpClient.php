<?php

namespace App\Service;

use App\Entity\Credentials;

class HttpClient
{
    public function pullResource(string $url, Credentials $credential = null)
    {
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);

        if ($credential) {
            curl_setopt($ch, CURLOPT_HTTPHEADER,
                [
                    'X-User-name: ' . $credential->getUsername(),
                    'X-User-password: ' . $credential->getPassword(),
                ]
            );
        }

        $content = curl_exec($ch);
        curl_close($ch);

        return $content;
    }

    public function pushResource(string $url, string $data, Credentials $credential = null)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if ($credential) {
            curl_setopt($ch, CURLOPT_HTTPHEADER,
                [
                    'X-User-name: ' . $credential->getUsername(),
                    'X-User-password: ' . $credential->getPassword(),
                ]
            );
        }

        $server_output = curl_exec($ch);

        curl_close ($ch);

        return $server_output;
    }
}