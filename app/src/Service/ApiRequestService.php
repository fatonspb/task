<?php

namespace App\Service;

use App\Entity\Credentials;

class ApiRequestService
{
    /**
     * @var string
     */
    private const PUSH_ENDPOINT = '/pull/datum';
    /**
     * @var string
     */
    private const PULL_ENDPOINT = '/pull/datum/';

    /**
     * @var DatumService
     */
    private $datumService;
    /**
     * @var HttpClient
     */
    private $httpClient;
    /**
     * @var string[]
     */
    private $log;
    /**
     * @var bool
     */
    private $isDebug;

    public function __construct(
        DatumService $datumService,
        HttpClient $httpClient,
        bool $isDebug = false
    ) {
        $this->datumService = $datumService;
        $this->httpClient = $httpClient;
        $this->isDebug = $isDebug;
    }

    /**
     * @return string[]
     */
    public function getLog(): array
    {
        return $this->log;
    }

    /**
     * @param Credentials $credential
     * @throws \Exception
     */
    public function makeApiRequests(Credentials $credential): void
    {
        $pushUrl = $credential->getUrl() . self::PUSH_ENDPOINT;
        $this->log('pushUrl', $pushUrl);

        // create resource
        $data = json_encode(['datum' => 'value']);

        $createResponse = $this->isDebug
            ? $createResponse = '{"datum_id":"' .  random_int(1, 100) . '"}'
            : $this->httpClient->pushResource($pushUrl, $data, $credential);

        $this->log('createResponse', $createResponse);
        $resourceId = json_decode($createResponse, true)['datum_id'] ?? null;

        // read resource
        if ($resourceId) {
            $pullUrl = $credential->getUrl() . self::PULL_ENDPOINT . $resourceId;
            $this->log('pullUrl', $pullUrl);

            $readResponse = $this->isDebug
                ? '{"secret":"' .  random_int(1, 100) . '"}'
                : $this->httpClient->pullResource($pullUrl);

            $this->log('readResponse', $readResponse);
            $resourceSecret = json_decode($readResponse, true)['secret'] ?? null;

            if ($resourceSecret) {
                $saved = $this->datumService->create($resourceId, $resourceSecret);
                $datumLogText = "id={$resourceId}, secret={$resourceSecret}";

                if ($saved) {
                    $this->log('Add new datum created', $datumLogText);
                } else {
                    $this->log('Can`t save datum', $datumLogText);
                }
            } else {
                $this->log('Response format error', 'ex:  {"secret":"value"}');
            }
        } else {
            $this->log('Response format error', 'ex: {"datum_id":"123"}');
        }
    }

    /**
     * @param string $param
     * @param $value
     * @throws \Exception
     */
    private function log(string $param, $value): void
    {
        $printableValue = is_string($value)
            ? $value
            :'NULL';

        $this->log[] = (new \DateTime())->format('r') . ': ' . $param . ': ' . $printableValue;
    }
}