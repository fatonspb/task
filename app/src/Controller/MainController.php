<?php

namespace App\Controller;

use App\Entity\Credentials;
use App\Repository\CredentialsRepository;
use App\Service\ApiRequestService;

class MainController extends BaseController
{
    /**
     * @var CredentialsRepository
     */
    private $credentialRepository;
    /**
     * @var ApiRequestService
     */
    private $apiRequestService;

    public function __construct(
        CredentialsRepository $credentialRepository,
        ApiRequestService $apiRequestService
    ) {
        $this->credentialRepository = $credentialRepository;
        $this->apiRequestService = $apiRequestService;
    }

    public function listOfCredentialAction():string
    {
        $response = "<h2>List of credentials:</h2>\n<ul>";
        $credentials = $this->credentialRepository->find([]);

        /** @var Credentials $credential */
        foreach ($credentials as $credential) {
            $response .= '<li>'
                . $credential->getUrl() . ' '
                . '<a href="?route=perform&id=' . $credential->getId() . '" class="btn btn-primary btn-sm">'
                    . 'Send request'
                . '</a>'
                . ' <a href="?route=perform&id=' . $credential->getId() . '&debug=1" class="btn btn-primary btn-sm">'
                    . 'Send debug request'
                . '</a>'
            . '</li>';
        }

        $response .= '</ul>';

        return $this->wrapHtml($response);
    }

    /**
     * @param int $id
     * @return string
     */
    public function performAction(int $id): string
    {
        $credentials = $this->credentialRepository->find(['id' => $id]);

        if (empty($credentials)) {
            $response = 'Credential not found';
        }

        /** @var Credentials $credential */
        foreach ($credentials as $credential) {
            $this->apiRequestService->makeApiRequests($credential);
        }

        $response = implode("\n<br>", $this->apiRequestService->getLog());

        return $this->wrapHtml($response);
    }
}