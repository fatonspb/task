<?php

namespace App\Controller;

abstract class BaseController
{
    /**
     * @param string $html
     * @return string
     */
    public function wrapHtml(string $html): string
    {
        $head = '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">';

        return "<html>\n" . $head ."\n<body>\n" . $this->contentWrapper($html) . "\n</body>\n</html>";
    }

    /**
     * @param string $html
     * @return string
     */
    public function contentWrapper(string $html): string
    {
        return "\n<div class=\"my-5 jumbotron container\">\n" . $html . "\n</div>\n";
    }
}