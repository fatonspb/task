<?php

namespace App\Controller;

use App\Entity\Credentials;
use App\Entity\Datum;
use App\Repository\CredentialsRepository;
use App\Service\ApiRequestService;
use App\Service\DatumService;
use App\Service\DbDriver\DbDriverInterface;
use App\Service\HttpClient;

class ErrorController extends BaseController
{
    /**
     * @param string $msg
     * @param string|null $helpText
     * @return string
     */
    public function showErrorAction(string $msg, string $helpText = null): string
    {
        return $this->wrapHtml('<h1>Exception</h1>' . $this->errorWrapper($msg) . '<p>' . $helpText . '</p>');
    }

    /**
     * @param string $msg
     * @return string
     */
    private function errorWrapper(string $msg): string
    {
        return '<div class="alert alert-danger" role="alert">' . $msg . '</div>';
    }
}