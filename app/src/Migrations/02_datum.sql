-- datum: table
CREATE TABLE `datum`
(
    `id`     int(11)      NOT NULL AUTO_INCREMENT,
    `secret` varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;