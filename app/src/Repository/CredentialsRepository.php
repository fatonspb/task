<?php

namespace App\Repository;

use App\Entity\Credentials;

class CredentialsRepository extends BaseRepository
{
    /**
     * {@inheritDoc}
     */
    public function getTableName(): string
    {
        return Credentials::TABLE_NAME;
    }
}