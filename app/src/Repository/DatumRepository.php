<?php

namespace App\Repository;

use App\Entity\Datum;

class DatumRepository extends BaseRepository
{
    /**
     * {@inheritDoc}
     */
    public function getTableName(): string
    {
        return Datum::TABLE_NAME;
    }
}