<?php


namespace App\Repository;

interface RepositoryInterface
{
    /**
     * Find by simple WHERE criteria
     *
     * Result SQL:
     * WHERE `columnName` = 'singleValue' AND `columnName2` IN ('value1', 'value2') AND `columnName3` IS NULL
     *
     * @param array $criteria
     * $criteria = [
     *      'columnName' => 'singleValue',
     *      'columnName2' => ['value1', 'value2'],
     *      'columnName3' => null,
     * ];
     * @param string $order ('ASC' or 'DESC')
     * @param int $limit (0 for no limit)
     * @param int $offset
     */
    public function find(
        array $criteria,
        string $order = null,
        int $limit = null,
        int $offset = null
    );

    /**
     * Table name
     *
     * @return string
     */
    public function getTableName(): string;
}