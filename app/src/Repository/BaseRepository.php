<?php

namespace App\Repository;

use App\Entity\Credentials;
use App\Service\DbDriver\DbDriverInterface;

abstract class BaseRepository implements RepositoryInterface
{
    /**
     * @var DbDriverInterface
     */
    private $driver;

    public function __construct(
        DbDriverInterface $driver
    ) {
        $this->driver = $driver;
    }

    /**
     * {@inheritDoc}
     */
    public function find(
        array $criteria,
        string $order = null,
        int $limit = null,
        int $offset = null
    ) {
        $tableName = static::getTableName();
        $className = Credentials::class;

        return $this->driver->find($className, $tableName, $criteria, $order, $limit, $offset);
    }


}