<?php

namespace App\Entity;

class Datum
{
    /**
     * @var string
     */
    const TABLE_NAME = 'datum';

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $secret;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Datum
     */
    public function setId(int $id): Datum
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getSecret(): string
    {
        return $this->secret;
    }

    /**
     * @param string $secret
     * @return Datum
     */
    public function setSecret(string $secret): Datum
    {
        $this->secret = $secret;

        return $this;
    }
}