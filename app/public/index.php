<?php
define('ENV', getenv('ENV') ?? 'dev');
define('DOC_ROOT', getenv('DOC_ROOT') ?? $_SERVER['DOCUMENT_ROOT']);

if (ENV === 'dev') {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

require_once DOC_ROOT . '/vendor/autoload.php';

use App\Controller\ErrorController;
use App\Controller\MainController;
use App\Repository\CredentialsRepository;
use App\Repository\DatumRepository;
use App\Service\ApiRequestService;
use App\Service\DatumService;
use App\Service\DbDriver\MySql;
use App\Service\DbDriver\SqlException;
use App\Service\HttpClient;

$isDebug = $_GET['debug'] ?? 0;

// simple objects
$driver = new MySql();
$httpClient = new HttpClient();

// dependent objects
$credentialRepository = new CredentialsRepository($driver);
$datumRepository = new DatumRepository($driver);
$datumService = new DatumService($driver, $datumRepository);
$apiRequestService = new ApiRequestService($datumService, $httpClient, $isDebug);

// router
$indexRouteKey = 'index';
$route = $_GET['route'] ?? $indexRouteKey;

try {
    $controller = new MainController($credentialRepository, $apiRequestService);

    switch ($route) {
        case 'perform':
            $id = $_GET['id'] ?? null;

            $response = $controller->performAction($id);
            break;

        case $indexRouteKey:
        default:
            $response = $controller->listOfCredentialAction();
    }

    echo $response;
} catch (Exception $e) {
    $errorController = new ErrorController();

    $helpText = ($e instanceof SqlException)
        ? 'Did you run migrations?'
        : null;

    echo $errorController->showErrorAction($e->getMessage(), $helpText);
}

