<?php
define('ENV', getenv('ENV') ?? 'dev');
define('DOC_ROOT', getenv('DOC_ROOT') ?? $_SERVER['DOCUMENT_ROOT']);

if (ENV === 'dev') {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

require_once DOC_ROOT . '/vendor/autoload.php';

use App\Service\DbDriver\MySql;

switch ($argv[1] ?? null) {
    case 'migration':
        $driver = new MySql();
        $migrationsDir = DOC_ROOT . '/src/Migrations';
        $migrations = array_slice(scandir($migrationsDir), 2);

        foreach ($migrations as $migrationFile) {
            $migrationSql = file_get_contents($migrationsDir . '/' . $migrationFile);
            $driver->runSql($migrationSql);
        }

        break;

    default:
        echo "supported commands:\n\t'migration' to run migrations;\n";
}
